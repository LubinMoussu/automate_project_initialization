import os

import requests
from git import Repo

import app

# Define variables

local_dir = os.path.dirname(app.ROOT_DIR)
project_name = app.config.PROJECT_NAME
gitlab_token = app.config.GITLAB_TOKEN
gitlab_url = app.config.GITLAB_URL
namespace_name = app.config.NAMESPACE_NAME

# Step 1: Create a local directory
os.makedirs(local_dir, exist_ok=True)
print(f"Local directory '{local_dir}' created.")

# Step 2: Create a GitLab project
headers = {
    'PRIVATE-TOKEN': gitlab_token
}

namespace_response = requests.get(f'{gitlab_url}/api/v4/namespaces', headers=headers)
if namespace_response.status_code == 200:
    namespaces = namespace_response.json()
    namespace_id = None
    for namespace in namespaces:
        if namespace['path'] == namespace_name:
            namespace_id = namespace['id']
            break
    if namespace_id is None:
        print("Namespace not found.")
        exit(1)
else:
    print("Failed to fetch namespaces.")
    print(namespace_response.json())
    exit(1)

data = {
    'name': project_name,
    'namespace_id': namespace_id
}
response = requests.post(f'{gitlab_url}/api/v4/projects', headers=headers, data=data)

if response.status_code == 201:
    project_info = response.json()
    print(f"GitLab project '{project_name}' created.")
else:
    print("Failed to create GitLab project")
    print(response.json())
    exit(1)

# Step 3: Initialize a local Git repository and link it to the GitLab project
repo = Repo.init(local_dir)
origin = repo.create_remote('origin', project_info['http_url_to_repo'])

# Create basic files
with open(os.path.join(local_dir, 'README.md'), 'w') as f:
    f.write(f"# {project_name}\n")

with open(os.path.join(local_dir, '.gitignore'), 'w') as f:
    f.write('.idea/\n__pycache__/\n*.pyc\n')

# Add and commit initial files
repo.index.add(['README.md', '.gitignore'])
repo.index.commit('Initial commit')

# Push to GitLab
repo.git.push('--set-upstream', 'origin', 'master')
print(f"Initial commit pushed to '{project_name}' on GitLab.")

print("Setup complete.")
