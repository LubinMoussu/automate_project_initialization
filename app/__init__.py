import os

import yaml
from munch import munchify

SRC_DIR = os.path.dirname(os.path.abspath(__file__))
ROOT_DIR = os.path.dirname(SRC_DIR)

conf_file = os.path.join(SRC_DIR, "config.yaml")
# Read YAML file
with open(conf_file) as stream:
    data_loaded = yaml.safe_load(stream)
config = munchify(data_loaded)
