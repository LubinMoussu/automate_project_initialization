# Standard library imports
import logging
import time

# Local application imports
from unittest import TestCase

from app.initiate_project import InitiateProject

logger = logging.getLogger(__name__)


class TestInitiateProject(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.init_project = InitiateProject(name="project1")

    def test_NormalizeString_GivenCamelCaseString_ShouldReturnSnakeCase(self):
        string_list = [
            ("camel2_camel2_case", "camel2_camel2_case"),
            ("getHTTPResponseCode", "get_http_response_code"),
            ("HTTPResponseCodeXYZ", "http_response_code_xyz"),
            ("HTTPResponseCode-xyz", "http_response_code_xyz"),
            ("HTTPResponseCode xyz", "http_response_code_xyz"),
        ]
        for string, expected in string_list:
            actual = self.init_project.normalize_string(string)
            self.assertEqual(actual, expected)
