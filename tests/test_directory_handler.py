# Standard library imports
import logging
import os
from unittest import TestCase

from app.directory_handler import DirectoryHandler

# Local application imports

logger = logging.getLogger(__name__)


class TestDirectoryHandler(TestCase):
    def setUp(self) -> None:
        self.directory_handler = DirectoryHandler(project_name="TestProject")
