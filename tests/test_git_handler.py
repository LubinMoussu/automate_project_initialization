# Standard library imports
import logging
import pprint

# Local application imports
from unittest import TestCase

from app.git_handler import GitHandler

logger = logging.getLogger(__name__)


class TestGitHandler(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.git_handler = GitHandler(name="RedWineQualityModeling")

    def test_InstantiateGitHandler_(self):
        project = self.git_handler.gl.projects.get("25640172")
        actual = project._attrs["name"]
        expected = "RedWineQualityModeling"
        self.assertEqual(actual, expected)
