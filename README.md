AutomateProjectInitialization is a script that allows to initiate a git repository and a local project and link both together

## Prerequisites
Before you begin, ensure you have met the following requirements:
Python==3.7

## Using AutomateProjectInitialization
To use AutomateProjectInitialization, follow these steps:
python run.py -i <name_of_project>